import React, { useEffect, useState } from 'react';

function ConferenceForm( ){
    const [locations, setLocations] = useState ([]);
    const fetchData = async () => {
        const locationUrl = 'http://localhost:8000/api/locations/';

        const locationResponse = await fetch(locationUrl);

        if (locationResponse.ok){
            const data = await locationResponse.json();
            setLocations(data.locations);
        }
    }

    const[name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const[starts, setStarts] = useState('');
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value)
    }

    const[ends, setEnds] = useState('');
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value)
    }

    const[description, setDescription] = useState('');
    const handleDescriptionChange = (event) =>{
        const value = event.target.value;
        setDescription(value)
    }

    const[maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value)
    }

    const[maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value)
    }

    const[location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
            }
        }
    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">

                    {/* name */}
                    <div className="form-floating mb-3">
                        <input
                        onChange={handleNameChange}
                        placeholder="Name"
                        required type="text"
                        name ="name"
                        id="name"
                        className="form-control"
                        />
                        <label htmlFor="name">Name</label>
                    </div>

                    {/* starts date */}
                    <div className="form-floating mb-3">
                        <input
                        onChange={handleStartsChange}
                        type="date"
                        id="start"
                        name="starts"
                        value={starts}
                        min="2023-02-23"
                        max="2027-12-31"
                        className="form-control"
                        />
                        <label htmlFor="starts">Starts</label>
                    </div>

                    {/* ends date */}
                    <div className="form-floating mb-3">
                        <input
                        onChange={handleEndsChange}
                        type="date"
                        id="ends"
                        name="ends"
                        value={ends}
                        min="2023-02-23"
                        max="2027-12-31"
                        className="form-control"
                        />
                        <label htmlFor="ends">Ends</label>
                    </div>

                    {/* description */}
                    <div className="mb-3">
                        <label htmlFor="description">Description</label>
                        <textarea
                        onChange={handleDescriptionChange}
                        required type="text"
                        className="form-control"
                        name="description"
                        id="description"
                        rows="3">
                        </textarea>
                    </div>

                    {/* max presentnations */}
                    <div className="form-floating mb-3">
                        <input
                        onChange={handleMaxPresentationsChange}
                        placeholder="Maximum Presentations"
                        required type="number"
                        name = "max_presentations"
                        id="max_presentations"
                        className="form-control"
                        />
                        <label htmlFor="max_presentations">Max Presentations</label>
                    </div>

                    {/* max attendees  */}
                    <div className="form-floating mb-3">
                        <input
                        onChange={handleMaxAttendeesChange}
                        placeholder="Maximum Presentations"
                        required type="number"
                        name = "max_attendees"
                        id="max_attendees"
                        className="form-control"
                        />
                        <label htmlFor="max_attendees">Max Attendees</label>
                    </div>

                    {/* Location */}
                    <div className="mb-3">
                        <select
                        onChange={handleLocationChange}
                        required id="location"
                        name ="location"
                        className="form-select">
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                              <option key={location.id} value ={location.id}>
                              {location.name}
                              </option>
                            );
                        })}
                        </select>
                    </div>

                    <button className="btn btn-primary">Create</button>

                    </form>
                </div>
            </div>
        </div>
    );
}
export default ConferenceForm;
