
function createCard(name, description, pictureUrl, newStart, newEnd,location) { //this is how we are rendering the information onto the webpage
    return `
        <div class="card shadow p-3 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer text-muted">
                ${newStart}-${newEnd}
            </div>
        </div>
    </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => { //wait until page is loaded

    const url = 'http://localhost:8000/api/conferences/'; //get API url
    const columns = document.querySelectorAll('.col')
    let colIndx = 0

    try {
      const response = await fetch(url); //wait until url is loaded

      if (!response.ok) {
        // Figure out what to do when the response is bad
            return `
            <div class="alert alert-danger" role="alert">
                A simple danger alert—check it out!
            </div>
            `

      } else {
        const data = await response.json(); //wait until it turns the data from API to json

        for (let conference of data.conferences) { //loop through each conference to create a new card
          const detailUrl = `http://localhost:8000${conference.href}`; //get the url to the conference which has the details of it
          const detailResponse = await fetch(detailUrl); // wait until we get this url loaded
          if (detailResponse.ok) {
            const details = await detailResponse.json(); // wait until the data is turned into json
            const title = details.conference.name; //get the name of the conference
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const startDate = new Date(details.conference.starts); //turned into an object so we can make changes
            const newStart = startDate.toLocaleDateString(); //changes the format of the date (this is what we are going to reference)

            const endDate = new Date(details.conference.ends);
            const newEnd= endDate.toLocaleDateString();

            const location = details.conference.location.name;

            const html = createCard(title, description, pictureUrl, newStart, newEnd, location); //createCard now has these parameters for a new card
            const column = columns[colIndx];
            column.innerHTML += html;
            colIndx = (colIndx + 1 ) % 3;
          }
        }
      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }
  });
