
window.addEventListener('DOMContentLoaded', () => { //wait until page is loaded
    const form = document.getElementById('login-form'); //grab the element by it's id that we want to work with
    form.addEventListener('submit', async event => { //listen for a submit
      event.preventDefault(); //prevent default behavior

      const fetchOptions = {
        method: 'post',
        body: new FormData(form), //login requires normal data not json; create new form with input and create key-value parirs; ex: {email: ex@gmail.com, password: password}
        credentials: 'include', //this is how it is authenication happens, we are able to do this because of corsheaders
        };

      const url = 'http://localhost:8000/login/'; //get the API
      const response = await fetch(url, fetchOptions); //wait for the url and the person to submit the log in
      if (response.ok) {
        window.location.href = '/'; //redirect back to home -> window.location represents the current URL of the page
      } else {
        console.error(response);
      }
    });
  });
