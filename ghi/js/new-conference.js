
window.addEventListener('DOMContentLoaded', async () => { // waiting until the page loads
    const locationUrl = 'http://localhost:8000/api/locations/'; //getting the location url
    const locationResponse = await fetch(locationUrl); //waiting until url is loaded

    if (locationResponse.ok){ //if okay
        const data = await locationResponse.json(); //wait until data from url is loaded into json

        const selectTag = document.getElementById('location'); //select the element by it's id we want to work with

        for (let location of data.locations){ //loop through the locations we have in our data
            const option = document.createElement('option'); //creating a new element called <option>
            option.value = location.id; //grabing the id of the location and assigning that the value of <option>
            option.innerHTML = location.name; //in the drop down menu, what is diplayed is the location name
            selectTag.appendChild(option); // append this new option location to the selectTag which is 'location'
        }
    }

    const formTag = document.getElementById('create-conference-form'); // Grab the element
    formTag.addEventListener('submit', async (event) => { //listening for a click then doing these things
        event.preventDefault(); //Prevent default behavior

        //getting the form data
        const formData = new FormData(formTag); //Get the data from the form using it's element and put it in entries in key pairs
        const json = JSON.stringify(Object.fromEntries(formData)); // turning formData object into an array then into a json string

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json', // this indicates that the request body is in JSON format.
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset(); //resets the form (clears it)
                const newLocation = await response.json();
            }
    });
});
